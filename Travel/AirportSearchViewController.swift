import Foundation
import UIKit

/**
 `AirportSearchViewController` provides an interface for searching and selecting an airport. Upon selection the controller call's it's callback block `didSelectAirport` with the
 selection and then dismisses itself.
 */
public final class AirportSearchViewController: UITableViewController {

    // MARK: UIViewController

    public override func viewDidLoad() {
        super.viewDidLoad()
        AirportDataStore.shared.findAirports(with: nil) { search, results in
            DispatchQueue.main.async {
                self.update(results: results.airports)
            }
        }
    }

    // MARK: UITableViewDataSource
    public override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return airports.count
    }

    public override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "search-id", for: indexPath)
        let airport = airports[indexPath.row]
        cell.textLabel?.text = airport.name
        cell.detailTextLabel?.text = airport.shortCode
        return cell
    }

    // MARK: UITableViewDelegate

    public override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        didSelectAirport(airports[indexPath.row])
        dismiss(animated: true, completion: nil)
    }

    /// Completion block called when user select's an airport from the tableview
    public var didSelectAirport: (Airport) -> Void = { _ in }

    private var airports = [Airport]()
    
    // MARK: Helper functions
    
    /// Should be called from the main thread as this updates the UI
    fileprivate func update(results: [Airport]) -> Void {
        self.airports = results
        self.tableView.reloadData()
    }
}

extension AirportSearchViewController: UISearchBarDelegate {
    public func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        DispatchQueue.global(qos: .userInitiated).async { [weak self] in
            guard let self = self else {
                return
            }
            AirportDataStore.shared.findAirports(with: searchText) {search, results in
                // Since the new data updates the UI, handle it on the main thread
                DispatchQueue.main.async {
                    // Results come in asynchronously and may be outdated
                    guard searchText == searchBar.searchTextField.text else {
                        print("Outdated query returned for: \(searchText)")
                        print("Current text is \(searchBar.searchTextField.text ?? "")")
                        return
                    }
                    self.update(results: results.airports)
                }
            }
        }
    }
}
