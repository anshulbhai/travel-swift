//
//  AirportDataStore.swift
//  Travel
//
//  Created by Anshul Kapoor on 11/2/19.
//  Copyright © 2019 Around. All rights reserved.
//

import Foundation

/// Store providing and caching search results from the network
public final class AirportDataStore {
    var cache: NSCache = NSCache<NSString, AirportResults>.init()
    
    /// Singleton access to an instance of `AirportDataStore`.
    public static let shared = AirportDataStore()
    
    private init() {
        // Limit can be tuned based on memory/responsiveness requirements
        cache.countLimit = 10
    }
    
    /// Abstract the search and return cached results if present
    func findAirports(with search: String?, completion: @escaping (String?, AirportResults) -> Void) -> Void {
        // Here we avoid caching for when search is nil as that would create a copy
        // of ALL of the airports
        if let searchString = search , let cachedResult = cache.object(forKey: searchString as NSString) {
            print("Search term was already in cache: \(searchString)")
            completion(searchString, cachedResult)
        } else {
            DispatchQueue.global(qos: .userInitiated).async {
                let results = AirportResults.init(results: AirportDataSource.shared.airportsMatching(search))
                if let searchString = search {
                    self.cache.setObject(results, forKey: searchString as NSString)
                }
                completion(search, results)
            }
        }
    }
}
