//
//  AirportResults.swift
//  Travel
//
//  Created by Anshul Kapoor on 11/2/19.
//  Copyright © 2019 Around. All rights reserved.
//

import Foundation

/// Helper class to wrap the results so we can cache them
class AirportResults {
    let airports: [Airport]
    
    init(results: [Airport]) {
        self.airports = results
    }
}
